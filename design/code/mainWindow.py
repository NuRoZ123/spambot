# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_windowMain(object):
    def setupUi(self, windowMain):
        if not windowMain.objectName():
            windowMain.setObjectName(u"windowMain")
        windowMain.resize(404, 300)
        self.logoQLabel = QLabel(windowMain)
        self.logoQLabel.setObjectName(u"logoQLabel")
        self.logoQLabel.setGeometry(QRect(260, 170, 131, 131))
        self.logoQLabel.setPixmap(QPixmap(u"D:/computer/download/logoNotEnd 128x128.png"))
        self.layoutWidget = QWidget(windowMain)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(10, 10, 371, 161))
        self.horizontalLayout = QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.timeWaitQlabel = QLabel(self.layoutWidget)
        self.timeWaitQlabel.setObjectName(u"timeWaitQlabel")

        self.verticalLayout.addWidget(self.timeWaitQlabel)

        self.closeQLabel = QLabel(self.layoutWidget)
        self.closeQLabel.setObjectName(u"closeQLabel")

        self.verticalLayout.addWidget(self.closeQLabel)

        self.spamQLabel = QLabel(self.layoutWidget)
        self.spamQLabel.setObjectName(u"spamQLabel")

        self.verticalLayout.addWidget(self.spamQLabel)

        self.betweenSpamQlabel = QLabel(self.layoutWidget)
        self.betweenSpamQlabel.setObjectName(u"betweenSpamQlabel")

        self.verticalLayout.addWidget(self.betweenSpamQlabel)

        self.nbExecQLabel = QLabel(self.layoutWidget)
        self.nbExecQLabel.setObjectName(u"nbExecQLabel")

        self.verticalLayout.addWidget(self.nbExecQLabel)


        self.horizontalLayout.addLayout(self.verticalLayout)

        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.timeWaitQSpinBox = QSpinBox(self.layoutWidget)
        self.timeWaitQSpinBox.setObjectName(u"timeWaitQSpinBox")
        self.timeWaitQSpinBox.setMaximum(15)

        self.verticalLayout_2.addWidget(self.timeWaitQSpinBox)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_4)

        self.closeQcheckBox = QCheckBox(self.layoutWidget)
        self.closeQcheckBox.setObjectName(u"closeQcheckBox")
        self.closeQcheckBox.setChecked(True)

        self.verticalLayout_2.addWidget(self.closeQcheckBox)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_3)

        self.phraseQLineEdit = QLineEdit(self.layoutWidget)
        self.phraseQLineEdit.setObjectName(u"phraseQLineEdit")

        self.verticalLayout_2.addWidget(self.phraseQLineEdit)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.doubleSpinBox = QDoubleSpinBox(self.layoutWidget)
        self.doubleSpinBox.setObjectName(u"doubleSpinBox")
        self.doubleSpinBox.setCursor(QCursor(Qt.ArrowCursor))
        self.doubleSpinBox.setMaximum(10.000000000000000)
        self.doubleSpinBox.setStepType(QAbstractSpinBox.AdaptiveDecimalStepType)
        self.doubleSpinBox.setValue(0.010000000000000)

        self.verticalLayout_2.addWidget(self.doubleSpinBox)

        self.nbExecQSpinBox = QSpinBox(self.layoutWidget)
        self.nbExecQSpinBox.setObjectName(u"nbExecQSpinBox")
        self.nbExecQSpinBox.setMinimum(1)
        self.nbExecQSpinBox.setMaximum(99999)
        self.nbExecQSpinBox.setValue(10)

        self.verticalLayout_2.addWidget(self.nbExecQSpinBox)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.widget = QWidget(windowMain)
        self.widget.setObjectName(u"widget")
        self.widget.setGeometry(QRect(30, 240, 211, 26))
        self.horizontalLayout_2 = QHBoxLayout(self.widget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.closeQpushButton = QPushButton(self.widget)
        self.closeQpushButton.setObjectName(u"closeQpushButton")

        self.horizontalLayout_2.addWidget(self.closeQpushButton)

        self.startQpushButton = QPushButton(self.widget)
        self.startQpushButton.setObjectName(u"startQpushButton")

        self.horizontalLayout_2.addWidget(self.startQpushButton)


        self.retranslateUi(windowMain)
        self.closeQpushButton.clicked.connect(windowMain.close)

        QMetaObject.connectSlotsByName(windowMain)
    # setupUi

    def retranslateUi(self, windowMain):
        windowMain.setWindowTitle(QCoreApplication.translate("windowMain", u"Spam Bot - NuRoZ", None))
        self.logoQLabel.setText("")
        self.timeWaitQlabel.setText(QCoreApplication.translate("windowMain", u"temps d'attente avant depart:", None))
        self.closeQLabel.setText(QCoreApplication.translate("windowMain", u"fermer cette fenetre lors de l'execution:", None))
        self.spamQLabel.setText(QCoreApplication.translate("windowMain", u"saisir la phrase a spam:", None))
        self.betweenSpamQlabel.setText(QCoreApplication.translate("windowMain", u"temps entre chaque spam:", None))
        self.nbExecQLabel.setText(QCoreApplication.translate("windowMain", u"nombre d'exucution du message: ", None))
        self.closeQcheckBox.setText("")
        self.phraseQLineEdit.setPlaceholderText(QCoreApplication.translate("windowMain", u"phrase a spam", None))
        self.closeQpushButton.setText(QCoreApplication.translate("windowMain", u"quitter !", None))
        self.startQpushButton.setText(QCoreApplication.translate("windowMain", u"start !", None))
    # retranslateUi

