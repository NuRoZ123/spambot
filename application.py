import sys

from pynput.keyboard import Key, Controller
from time import sleep
from PySide6.QtWidgets import QApplication, QMainWindow
from keyboard import is_pressed
from design.code.mainWindow import Ui_windowMain

class Window(QMainWindow):
    """Classe de l'application princpale

        Attributs d'instance
        --------------------
        ui :
            l'interface graphique de l'application
        """


    def __init__(self):
        super(Window, self).__init__()

        self.ui =  Ui_windowMain()
        self.ui.setupUi(self)
        self.ui.startQpushButton.clicked.connect(self.start)
        self.ui.logoQLabel.setPixmap("./logo.png")

    def start(self) -> None:
        if self.ui.closeQcheckBox.isChecked():
            self.showWidow(False)

        clavier = Controller()

        sleep(self.ui.timeWaitQSpinBox.value())
        if self.ui.phraseQLineEdit.text() == "":
            phrase = "phrase a spam"
        else:
            phrase = self.ui.phraseQLineEdit.text()

        for i in range(0, self.ui.nbExecQSpinBox.value()):
            for uneLettre in phrase:
                clavier.press(uneLettre)
                clavier.release(uneLettre)

            clavier.press(Key.enter)
            clavier.release(Key.enter)
            sleep(self.ui.doubleSpinBox.value())
            if is_pressed("esc"): break


        self.showWidow(True)

    def showWidow(self, etat: bool) -> None:
        if etat:
            self.show()
        else:
            self.hide()



## PROGRAMME PRINCIPAL ##
if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = Window()
    window.show()

    sys.exit(app.exec_())

